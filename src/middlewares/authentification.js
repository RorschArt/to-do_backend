	//NPM
const jwt = require('jsonwebtoken')
	//Model
const User = require('../models/user.js')

//Middleware De verification du JSONToken Fourni par le client
const auth = async (req, res, next) => {
	try {
		//on Recupere le token fourni par le client depuis le header 'authorization'
		//de la requete HTTP, on enleve le mot bearer de la clef
		// la moindre erreur fourni a verify renvoi dans le catch, il faut donc
		//s'assurer que la variable token est parfaitement fiable
		const token = req.header('Authorization').replace('Bearer ', '')
		const decoded = jwt.verify(token, process.env.JSON_SECRET)
		// on essaye de recuperer l'utilisateur avec l'_id noter dans le token
		// lors de sa creation dans /models/user.js
		//@url: https://mongoosejs.com/docs/api.html#model_Model.findOne
		const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })

		if (!user) {
		//Renvoi dans le catch si l'utilisateur n'est pas toruve
			throw new Error()
		}
		//On stocke le JSONtoken dans la requete pour pouvoir l'utiliser dans
		//le router logout par la suite.
		req.token = token
		//l'identification s'est bien deroulee, on peut remvoyer vers le routeur user.
		//On a deja fait la recherche d'un utilisateur, on peux eviter de refaire
		//l'operation dans le routeur qui suit notre middleware, on stocke les
		//infos dans la requete pour pouvoir les recuperer
		req.user = user
		next()
	} catch (e) {
		res.status(401).send({ error: 'Mauvaise Identification (JSONWebToken).' })

	}
}

module.exports = auth
