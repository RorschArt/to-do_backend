const sgMail = require('@sendgrid/mail')

const sendGridAPIKey = process.env.SENDGRID_API_KEY

sgMail.setApiKey(sendGridAPIKey)


const sendWelcomEmail = (email, name) => {

    const msg = {
        to: email,
        from: 'litooo@hotmail.fr',
        subject: `Bienvenue Au Greta-2019 ${name} !`,
        text: `Nous Vous Souhaitons Une Agreable Utilisation De notre Application To-Do. La Creme De la Creme des Developpers ! Tu vas Etre Eblouis !`,
    }

    sgMail.send(msg)
}

const sendDeleteAccount = (email, name) => {
    const msg = {
        to: email,
        from: 'litooo@hotmail.fr',
        subject: `C\'est si Triste de Te Voir Partir ${name} ! :(`,
        text: `Tu T\'appretes A Quitter La Creme De La Creme Des developpeurs, Tu Vas Surement Le Regretter ... Alors Tu Veux Toujours ?`  }
    sgMail.send(msg)
}

//es6 shorthand {sendWelcomEmail:sendWelcomEmail}
module.exports = {
    sendDeleteAccount,
    sendWelcomEmail
}
