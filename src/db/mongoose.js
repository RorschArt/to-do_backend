const mongoose = require('mongoose')
const validator = require('validator')

//connection a notre base de donnee localhost Mongodb, port habituel Mongodb
//Documentation: @url: https://mongoosejs.com
//il est recommander d'utiliser 127.0... et non localhost car cela cause des
//erreur et des ralentissements, les raisons sont encore inconnus
//connection Mongoose : @url: https://mongoosejs.com/docs/connections.html#options
mongoose.connect(process.env.MONGODB_URL, {
	useNewUrlParser: true,
	useCreateIndex: true,
	//Eviter les messages d'erreur de deprecations
	useFindAndModify: false
})
