//NPM
const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');
//Models
const Task = require('./task')

	//A noter que Mongoose va toujours rajouter un S a nos noms de modeles, donc il
	// faut le noter au singulier ici
	//@url: https://mongoosejs.com/docs/models.html
	//@url: https://mongoosejs.com/docs/api.html#schema_Schema
	//Validation / Sanitization @url: https://mongoosejs.com/docs/validation.html
const userSchema = new mongoose.Schema({
	name: {
		type: String,
		trim: true
	},
	age: {
		type: Number,
		default:0,
		trim: true,
		validate(value) {
			if (value < 0) {
				throw new Error('Age ne peut pas etre en dessous de 0 !')
			}
		}
	},
	email: {
		type: String,
	//l'email sera unique, ne pas permettre de creer de compte avec meme email
		unique:true,
		trim: true,
		required:true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw new Error('Email non Valide !')
			}
		}
	},
	password: {
		type: String,
		trim: true,
		minlength: 5,
		required: true
	},
	tokens:[{
		token: {
			type: String,
			required: true
		}
	}],
    avatar: {
        type: Buffer
    }
},
    //on ajoute les timestamp
{
    timestamps: true

})

    // Mongoose donne la possibilite de creer un systeme de relation entre
    // collection a la facon SQL
    //On fait comprendre a Mongoose la relation entre nos collection
    //Lorsque l'on creer une instance nous n'auront jamais en dur le document
    // venant de Task dans la propriete d'un User, c'est 'virtuel'
    //@url: https://mongoosejs.com/docs/guide.html#virtuals
    userSchema.virtual('tasks', {
    // collection A relier
        ref: 'Task',
    //on set la clef local qui relie note collection a la collection Task
        localField: '_id',
    //On set la clef etrangere qui relie notre collection a la collection Task
        foreignField: 'owner'
    })

	//@url: https://www.npmjs.com/package/jsonwebtoken
	//creation d'un token avec sign({}, Phrase secrete choisie pour etre sur que
	// cela n'a pas ete altere et non alterable par qqn avec le jsonToken, {options})
	//voici un exemple d'un token 1ww232xs.1w1w1wx.1w1w1 ou header.body.signature
	//		represente -->			 type . data  . verif
	//@url: https://www.base64decode.org/  --> permet de decoder la partie data du JSON

	// Il faut assigner un JsonToken lors de la connexion d'un utilisateur
	//On creer un fonction pour cela dans l'objet schema.methods
	//Ainsi la fonction est accessible dans les instances de User
	// @url: https://mongoosejs.com/docs/guide.html#methods
	userSchema.methods.generateAuthToken = async function() {
	// pour une meilleure lisibilite
		const user = this
		const token = jwt.sign({ _id: user._id.toString() }, process.env.JSON_SECRET)
	//@url: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat
	//ajoute des valeur dans un tableau, non pas un tableau dans un tableau ici
		user.tokens = user.tokens.concat({ token })
		await user.save()

		return token
	}
		//'toJson' viens de la librairie Mongoose et permet de stringify l'object
		//@url: https://mongoosejs.com/docs/guide.html#toJSON

		// on ne veut pas renvoyer les donnees sensibles (password, tokens) vers
		// le client, le code qui suit s'appliquera a chaque instances. On rajoute
		// donc du code a la methode, qui est appele a chqaue res.send() du router
	userSchema.methods.toJSON = function () {
		//Pour une meilleur lisibilitee
		const user = this
		//On converti en objet
		//@url: https://mongoosejs.com/docs/guide.html#toObject
		const userObject = user.toObject()
		//On utilise l'operateur delete pour supprimer les proprietes qui doivent
		//rester privees et ne pas les renvoyer au client
		delete userObject.password
		delete userObject.tokens
        delete userObject.avatar

		return userObject
	}


	// Creer une fonction dans static fonctionne comme PHP(berk), on y a acces
	// en appelant User, different de methode qui est accessible seulement depuis
	//l'instance
	//@url: https://mongoosejs.com/docs/api.html#schema_Schema-static
	userSchema.statics.findByCredentials = async (email, password) => {
	// ici {email} est un shorthand syntax, en realite {email:email}
		const user = await User.findOne({email})
	// pas d'utilisateur a cette adress on arrete l'execution de la fonction
		if (!user) {
	// il est preferable de ne pas donner d'infos trop specifique(hacking)
			throw new Error('Connexion echouée')
		}
		//retourne true ou false en fonction de la comparaison
		const isMatch =  await bcrypt.compare(password, user.password)

		if (!isMatch) {
			throw new Error('Connexion echouée')
		}

		return user
	}

    //Un middleware permet de creer une operation n'importe ou pour eviter de
	//devoir coder l'operation a de multiples endroits, donc un code plus clean
	//@url: https://medium.com/@selvaganesh93/how-node-js-middleware-works-d8e02a936113

	// HASHAGE Mot de Passe
	//On hash son mot de passe AVANT (.pre) de sauvegarder un utilisateur
userSchema.pre('save', async function (next) {
	//On ne peut pas utiliser une arrow function car this est important ici
	// this se refere a l'user en creation
	const user = this
	//Mongoose methode permettant de verifier si un MDP a deja ete hashed
	//@url: https://mongoosejs.com/docs/api.html#document_Document-isModified
	// true lorsqu'un utilisateur est creer pour la premiere fois ou lorsque le
	//MDP est change lors de la requete
	if (user.isModified('password')) {
	user.password = await bcrypt.hash(user.password, 8)
	console.log(' Hashage du Mot de Passe :', user.password,' ')
}
	//next permet de terminer la fonction, sans cela elle ne sarreterait pas (middleware)
	next()
} )

    //Middleware permettant de supprimer toutes les taches d'un utilisateur
    //schema.pre va donc executer une fonction avant de supprimer un utilisateur
    //Donc cette fonction va s'appliquer seulement lorsqu'un utilisateur est supprime
userSchema.pre('remove', async function (next) {
    // pour une meilleur lisibilite seulement
    const user = this

    await Task.deleteMany({ owner: user._id})

    next()
})

const User = mongoose.model('User', userSchema)

module.exports = User
