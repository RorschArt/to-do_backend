const mongoose = require('mongoose')
const validator = require('validator');

	//A noter que Mongoose va toujours rajouter un S a nos noms de modeles, donc il
	// faut le noter au singulier ici
	//@url: https://mongoosejs.com/docs/models.html
	//Validation / Sanitization @url: https://mongoosejs.com/docs/validation.html
const taskSchema = new mongoose.Schema({
    	intitule:{
    		type: String,
    		default: 'Nouvelle Tache',
    		minlength: 4,
    		trim: true,
    		required: true,
    		validate(value){
    			if (validator.isInt(value)) {
    				throw new Error('L\'intitule ne peut pas etre un nombre !')
    			}
    		}
    	},
    	fait: {
    		type: Boolean,
    		default: false,
    		required: false
    	},
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            required : true,
        //Mongoose donne la possibilite de donner un referent a une propriete, parfait
        // dans notre cas car on pourra aller chercher dans la base l'utilisateur qui
        //a creer la tache
        //@url: https://mongoosejs.com/docs/api.html#schematype_SchemaType-ref
            ref: 'User'
        }
    },
    //On ajoute les timestamps
    {
            timestamps: true
    }
)

const Task = mongoose.model('Task', taskSchema)
	//Export du modele Task
module.exports = Task
