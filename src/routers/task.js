const express = require('express')
    // npm
	//Modeles De la base de donnees
const Task = require ('../models/task.js')
    //middleware
const auth = require ('../middlewares/authentification.js')

	// Creation d'un nouveau Routeur, afin de pouvoir separer nos fichiers
	//@url: http://expressjs.com/en/api.html#express.router --> express
	//@url: http://expressjs.com/en/api.html#router.METHOD --> express
const router = new express.Router()

//================================= POST =======================================
	//@url: http://expressjs.com/en/api.html#app.post.method
	//@url: https://mongoosejs.com/docs/models.html --> Mongoose
	// async/await est une syntaxe es6 pour facilite l'utilisation des promises
	// cela permet de representer de maniere synchrone les promises qui sont des
	//fonctions asynchrones, cela resulte d'une meilleure lisibilitee
	//@url: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
router.post('/tasks', auth, async (req, res) => {
    console.log(' Requete POST (Tasks) Recue ')
    const task = new Task({
    //On utilise l'es6 syntaxe pour copier les proprietes de l'objet 'req.body'
    // dans notre fonction constructrice (merci es6 ! houra babel !)
    //@url: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
        ...req.body,
        owner: req.user._id
    })

	try {
		await task.save()
		res.status(201).send(task)
		console.log(' Tache', req.body.intitule, 'Enregistrée Dans BD ')
	} catch (e) {
		res.status(400).send(e)
		console.log('Une Erreur Serveur Est Survenue :\n', e)
	}
})

//================================= GET ALL ====================================
	//@url: http://expressjs.com/en/api.html#app.get.method --> Express
	//@url: https://mongoosejs.com/docs/api.html#model_Model.find --> Mongoose
router.get('/tasks',auth, async (req, res) => {
	console.log(' Requete GET (All Tasks) Recue',' ')

    const match = {}
    const sort = {}

    if(req.query.sortBy) {
    //Nous obtenons un string, on peux utiliser la method split
    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split
    //on utilise le charactere : donne par le client pour separer nos prop
    const keys = req.query.sortBy.split(':')
    //Avec un STRING on peux aussi utiliser les Brackets notation
    //on recupere le createdAt pour le mettre en propriete de sort puis on lui
    //donne la valeur desc ou asc selon le client
    //tout cela sera passer en parametre de populate() pour permetttre au client
    //d'interagir avec Mongoose
    sort[keys[0]] = keys[1]
    }

    if (req.query.fait) {
    //on recoit un string, avec cette syntaxe on retourne vrai ou faux en fonction
    //de sa valeur. ici le == est tres important.
    //@url: https://heyjavascript.com/javascript-string-to-boolean/
        match.fait = (req.query.fait === "true")
    }

	try {
        // autre approche Mongoose nous permet de rajouter virtuellement une
        // propriete a l'instance  provisoirement pour fetcher les documents
        //de la collection 'Task' car nous lui avons explicitement donner cette
        //relation dans /models/user.js dans mongoose. virtual() peut donc aller
        //chercher dans la propriete 'tasks' de user contenant tous les documents
        //Task appartenant a l'utilisateur et les filtrer ensuite avec 'match:'
        //@url: https://mongoosejs.com/docs/api.html#model_Model.populate
        await req.user.populate({
        // On definit le nom de la propriete,  ici 'tasks'
            path: 'tasks',
        // ! shorthand es6 syntaxe {match: match}
            match,
        //Mongoose donne l'option de pouvoir limiter les resultats et de
        //choisir a partir d'ou on veux commencer a les envoyer
            options: {
        //Pure JS on recoit un STRING donne par le client dans l'URL on le parse en INT
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
        //Permet de trier nos taches lors de l'envoi
        //! es6 shorthand {sort: sort}
                sort
            }
        }).execPopulate()

        //Ainsi notre user possedera  en effet une prop 'tasks' avec un tableau d'objets
        //contenant les taches creer par l'utilisateur, nous sommes encore une fois
        //grandement aide de notre middleware 'auth'

		res.send(req.user.tasks)

		console.log(' Liste des Taches Renvoyée',' ')

	} catch (e) {

		res.status(500).send(e)

		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
	}
})

//================================= GET One ====================================
	//@url: http://expressjs.com/en/api.html#app.get.method --> Express
	//@url: https://mongoosejs.com/docs/api.html#model_Model.findOne Mongoose
router.get('/tasks/:id', auth, async (req, res) => {
    console.log(' Requete GET (Une Task) Recue',' ')

	const _id = req.params.id

	try {
    // ! Shorthand syntaxe {_id: _id}
		const task = await Task.findOne({ _id, owner: req.user._id })

		if(!task) {
			return res.status(404).send({error: 'Tache Non Trouvée dans la Base de Donnée'})
		}

		res.send(task)
		console.log(' Tache', task.intitule,  ' Renvoyée ')

	} catch (e) {
		res.status(500).send(e)
			console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
	}
})

//================================= Update ====================================
	//@url: https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate --> Mongoose
router.patch('/tasks/:id', auth, async (req, res) => {
    console.log(' Requete PATCH Pour Tache, id:', req.params.id,' ')

	const _id = req.params.id
	// pure javascript
	const updates = Object.keys(req.body)
	const allowedUpdates = ['intitule', 'fait']
	// retourne true si toutes les proprietes de la requete sont existantes et permisent
	//es6 --> updates.every((update) =>  allowedUpdates.includes(update))
	const isValidoperation = updates.every((update) => {
		return allowedUpdates.includes(update)
	})

	try {

		// si c'est false, pas la peine de continuer (return)
		if(!isValidoperation) {
			return res.status(404).send({error:'Propriete Non Modifiable'})
		}
        // au lieu de faire un if statement pour verifier si l'id de la tache est
        //possedee par l'utilisateur, on se sert de findOne et de son parametre d'objet
        //qui retournera undefined si la tache n'est pas possede par l'utilisateur
        // ! shorthand {_id: _id}
        const task = await Task.findOne({ _id, owner: req.user._id })

		// on peut enregistrer la tache avec ses proprietes associees

		if (!task) {
			return res.status(404).send({error:'Tache Non trouvée'})
		}

        updates.forEach((update) => {
            return task[update] = req.body[update]
        })

        await task.save()

		res.send(task)
		console.log(' Intitulé Tache Mise a Jour En :', task.intitule,' ')

	} catch (e) {
		res.status(500).send(e)
		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
	}
})

//================================= Delete =====================================
	//@url: https://mongoosejs.com/docs/api.html#model_Model.findByIdAndRemove -->Mongoose
	//@url: http://expressjs.com/en/api.html#app.delete.method --> express
router.delete('/tasks/:id', auth, async (req, res) => {
    console.log(' Requete DELETE Pour Tache, par :', req.user.name,' ')

    const _id = req.params.id

	try {
        // au lieu de faire un if statement pour verifier si l'id de la tache est
        //possedee par l'utilisateur, on se sert de findOne et de son parametre d'objet
        //qui retournera undefined si la tache n'est pas possede par l'utilisateur
        //@url:https://mongoosejs.com/docs/api.html#model_Model.findOneAndDelete
        // es6 shorthand { _id: _id}
		const task = await Task.findOneAndDelete({_id, owner: req.user._id})

		if (!task) {
			return res.status(404).send({error: 'Tache Non Trouvée'})
		}

		res.send(task)

		console.log(' Tache avec intitule :', task.intitule,  ' Supprimée ')

	} catch (e) {
		res.status(500).send(e)

		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
	}
})
	//export du routeur tasks
module.exports = router
