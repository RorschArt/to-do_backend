const express = require('express');
	// npm
const bcrypt = require('bcryptjs')
const multer = require('multer')
const sharp = require('sharp')
	//Modeles De la base de donnees
const User = require ('../models/user.js')
	//Middlewares
const auth = require ('../middlewares/authentification.js')
    //SendGrid Email
const { sendWelcomEmail, sendDeleteAccount } = require ('../emails/account')
	// Creation d'un nouveau Routeur, afin de pouvoir separer nos fichiers et les
	// integrer au path d'express
	//@url: http://expressjs.com/en/api.html#express.router --> express
	//@url: http://expressjs.com/en/api.html#router.METHOD --> express
const router = new express.Router()

	// IMPORTANT lorsque l'on appel res.send() express applique automatiquement
	//JSON.stringify() sur l'objet envoyer a l'interieur

//========================= Creation User  =====================================\\
	//@url: http://expressjs.com/en/api.html#app.post.method
	//@url: https://mongoosejs.com/docs/models.html --> Mongoose

	// async/await est une syntaxe es6 pour facilite l'utilisation des promises .
    //cela permet de representer de maniere synchrone les promises qui sont des
	//fonctions asynchrones, cela resulte d'une meilleure lisibilitee pour nous
	//@url: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
router.post('/users', async (req, res) => {
	//@url: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
	const props = Object.keys(req.body)
	const allowedProp = ['name', 'email', 'password', 'age']
	const isValidProp = props.every((prop) => {
		return allowedProp.includes(prop)
	})

    const user = new User (req.body)
    console.log(' Requete POST (User) Recue ')

    try {
    	if (!isValidProp) {
    		return res.status(404).send({error:'Proprieté non Allouable'})
    	}

    	await user.save()

    	const token = await user.generateAuthToken()

        sendWelcomEmail(user.email, user.name)
        console.log(' Email Envoyé a :', user.email, ' ')

    	res.status(201).send({ user, token })
    	console.log(' Utilisateur',user.name, 'Enregistré ')

    } catch (e) {
    	res.status(500).send(e)
    	console.log('Une Erreur Serveur Est Survenue :\n', e)
}
})

//================================= GET  ==================================\\
	//@url: http://expressjs.com/en/api.html#app.get.method --> Express
	//@url: https://mongoosejs.com/docs/api.html#model_Model.find --> Mongoose
	//on passe un middleware dans les parametres, pour l'authentification (auth)
router.get('/users/me', auth, async (req, res) => {
    console.log(' Requete GET (User) Recue',' ')

	//Le middleware situe dans middlewares/authentification.js est deja aller
	//cherhcer les infos de l'utilisateur et les a stocker dans req.user
	res.send(req.user)
})

//================================= Update ===================================\\
	//@url: https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate --> Mongoose
router.patch('/users/me', auth, async (req, res) => {
    console.log(' Requete PATCH Pour User', req.user.name,' ')
	//Pure Javascript methode
	const updates = Object.keys(req.body)
	const allowedUpdates = ['email', 'age', 'password', 'name']
	// retourne true si les proprietes de la requete sont existants et permit
	// la boucle stop des le premier retour en false
//es6 --> updates.every((update) =>  allowedUpdates.includes(update))
	const isValidoperation = updates.every((update) => {
		return allowedUpdates.includes(update)
})

	if (!isValidoperation) {
		return res.status(404).send({error:'Proprieté Non Editable'})
	}

	try {

	//es6 updates.forEach((update) => user[update] = req.body[update])
		updates.forEach((update) => {
			return req.user[update] = req.body[update]
		})

		await req.user.save()

		res.send(req.user)

		console.log(
            ' Utilisateur', req.user.name, 'a été Mis a Jour sur ',
            updates
        )

	} catch (e) {
		res.status(500).send(e)
		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
	}
})
//================================= Delete ===================================\\
	//@url: https://mongoosejs.com/docs/api.html#model_Model.findByIdAndRemove -->Mongoose
	//@url: http://expressjs.com/en/api.html#app.delete.method --> express
router.delete('/users/me', auth, async (req, res) => {
    	try {
    //@url: https://mongoosejs.com/docs/api.html#model_Model-remove
        await req.user.remove()
        console.log(' Utilisateur', req.user.name,  ' Supprimé, Ainsi que ses Taches associées')

        sendDeleteAccount(req.user.email, req.user.name)
        console.log(' Email Envoyé a :', req.user.name, ' ')

		res.send(req.user)

	} catch (e) {
		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')

		res.status(500).send(e)
	}
})

//============================================================================\\
//============================== Connexion User ==============================\\
router.post('/users/login', async (req, res) => {
	console.log(' Requete POST (User) pour authentification recue ')
	try {
	//Methode findByCredentials() creer par nous meme dans models/user.js vivant
	// dans le prototype de User
		const user = await User.findByCredentials(req.body.email, req.body.password)
	//fonction creer dans models/user egalement mais a partir de schemas.methods()
		const token = await user.generateAuthToken()
	//method getPublicProfile() ajouter dans le 'proto' de l'instance dans models/user.js
	//Utilisation d'un shorthand {user, token} car {user:user, token:token}
		res.send({ user, token })
		console.log(' Connexion de l\'utilisateur', user.name, ' ')

	} catch (e) {
		res.status(400).send('Une erreur Est Survenue lors de l\'identification')
	}
})

	//Permettre a l'utilisateur de se deconnecter AVEC autorisation au prealable (auth)
router.post('/users/logout', auth, async (req, res) => {
	try {
	// on a toujours le token de la session en cours qui avait ete stocke dans notre
	//middleware 'auth' dans la requete 'req.token'

	//il faut supprimer le token utilise lors de connexion courante, on doit rechercher
	//dans le tableau d'objet 'tokens' de l'utilisateur tokens:{_id:'',token:''}
	//On utilise filter pour verifier chaque objet du tableau et retourner
	//seulement les tokens qui ne sont pas = a req.token de la sesssion en cours
	//@url:https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
		req.user.tokens = req.user.tokens.filter((token) => {
			return token.token !== req.token
		})

		console.log(' JSONToken de la Session En Cours Supprimé ')

		await req.user.save()
		res.send()

	} catch (e) {
		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
		res.status(500).send(e)
	}

})

router.post('/users/logoutAll', auth, async (req, res) => {
    console.log(' Requete POST (LogOut All) Recue ')

	try {
	// on vide le tableau de JSONToken car l'utilisateur veux se deconnecter
	//de toutes ses session (portable,pc,tablette etc)
		req.user.tokens = []
	// une fois tous les JSONToken on sauvegarde l'utilisateur dans Mongodb
		await req.user.save()

		res.send()

	} catch(e) {
		console.log(' Une Erreur Serveur Est Survenue :\n', e,' ')
		res.status(500).send()
	}
})
//========================= Avatar Upload Profile ===============================
    //@url:https://github.com/expressjs/multer#readme
const upload = multer({
    // Permet de limiter l'acceptation de certaine proprietes de l'upload
    limits: {
    //taille max
        fileSize: 10000000, //en bytes
    //type de fichier . ici 'cb' est une simple callback  cb(error, success)
    //file est un objet contenant les info de l'image uploader

    },
    fileFilter(req, file, callback) {
    //.match() accepte des regEx que l'on utilise pour le passage du test du type
    // de fichier @url:https://regex101.com
    // on a / expression /  .  \ permet d'echapper le sens des caractere speciaux qui ont
    // un traitement special en regex et $ permet de dire que c'est a la fin du mot a traiter
    // () permet de separer les traitements
        if (!file.originalname.match(/\.(png|jpg|svg|jpeg)$/)) {

            return callback( new Error ('Veuillez Fournir une Image de Type .png, .jpeg, .jpg ou .svg' ))
        }

        callback(undefined, true)

    }

})
    //upload.single() determine le nom de la clef que multer ira chercher pour
    //recevoir le fichier, c'est un middleware
    //On peut ajouter a express une deuxieme middleware, qui s'execute lors d''une
    // erreur, dans les parametres. La premiere est executer en cas de succces
router.post('/users/me/avatar', auth, upload.single('avatar'), async (req, res) => {
    console.log(' Requete POST (Avatar) Recue ')
    //multer (NPM) nous donne la possibilite d'acceder dans notre callback a l'objet qui
    //stocke les infos du fichier uploader par le client 'file'

    //Sharp (NPM) nous donne la possibilite de traiter les images recu en binaire(buffer)
    // On converti les donnees en buffer apres avoir fait notre conversion
    const buffer = await sharp(req.file.buffer).resize({width: 250, height: 250}).png().toBuffer()
    //Traitement termine, on peut le sauvegarder dans la BD
    req.user.avatar = buffer
    await req.user.save()
    res.status(201).send()
    //Sans un middleware pour handle l'erreur on aura en reponse au client une
    //erreur en format html, si on veux envoyer l'erreur en JSON on doit fournir
    //a express un second middleware qui handle le 'return new error' du middleware upload()
    //pour faire comprendre a express que l'on veut executer un middleware
    // il est important de fournir dans les params 4 arguments dont 'error'
}, (error, req, res, next) => {
    res.status(400).send({error: error.message})
})

router.delete('/users/me/avatar', auth, async (req, res) => {
    console.log(' Requete DELETE Pour Avatar, par :', req.user.name,' ')
    // on efface l'image binaire de l'utilisateur
        req.user.avatar = undefined
    // puis on enregistre le changement dans la db
        await req.user.save()
        res.send()
})
    //Requete Pour lire L'avatar d'un utilisateur
router.get('/users/:id/avatar', async (req, res) => {
    console.log(' Requete GET (Avatar User) Recue',' ')

    try {
        const user = await User.findById(req.params.id)

        if (!user || !user.avatar) {
            // Qqch s'est mal passe on envoie une erreur qui sera recu par le catch
            throw new Error()
        }
    //set() permet de determiner nous meme certaine propriete d'un header
    //si ce n'est pas specifier comme nous avons en general fait express le determine seul
    //Grace a Sharp on est sur que nos images seront toutes en .png
        res.set('Content-Type', 'image/png')
        res.send(user.avatar)
    } catch (e) {
        res.status(404).send(e)
    }

})

module.exports = router
