    //On creer un fichier separer car on veut avoir la possibilite de ne pas
    //appeler 'listen' lors de nos test avec superTest
    
    //On charge express
    const app = require('./app');
	//Stocker le port dans une variable, pour deploiement sur heroku ou autre plus tard
	// restera en port 3000 lors du developpement
    // les variables d'environnement sont stocker dans le dossier config
    const port = process.env.PORT
	// Permettre a notre localhost d'ecouter, pour pouvoir lui envoyer des requetes
	//@url: http://expressjs.com/en/api.html#app.listen
app.listen(port, async (req, res) => {
	console.log(' Server En Cours D\'execution et Ecoute sur le Port', port,' ')
})
