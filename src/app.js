	//connexion Base de donnee
require('./db/mongoose.js')
	//npm
const express = require('express')
	//Routeurs
const taskRouter = require ('./routers/task')
const userRouter = require ('./routers/user')
	//Contenir l'objet fourni par express dans une variable
const app = express()
	//Parser les requetes qui arrivent sans cela les requetes serait "vides"
	//@url: http://expressjs.com/en/api.html#app.use
app.use(express.json())
	//Ajouter les routeurs a com/en/api.html#app.use
app.use(taskRouter)
app.use(userRouter)

module.exports = app
