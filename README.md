# To-Do-App-Backend

  Ce Repo contiendra le code de notre partie serveur, La base de donnee est en *no-SQL* de type *MongoDB* avec *Express* et la librairie __O__ bject __D__ ocument __M__ apper __*Mongoose*__ qui nous permettra de __facilite__ la __manipulation__ et la __mise en structure__ de notre base de donnees.
---
## Pour Faire Bref  :
* [Documentation USER API](https://gitlab.com/litooo/to-do_backend/blob/master/user-front-end.md "README").
* [Documentation TASK API](https://gitlab.com/litooo/to-do_backend/blob/master/task-front-end.md "README").
* [Documentation Uploads](https://gitlab.com/litooo/to-do_backend/blob/master/uploads-front-end.md "README").
* Si vous Voulez Voir vos Requetes Il faut Au prealable installer Heroku CLI [Heroku CLI]( https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up "Site Officiel") puis ensuite faire : `heroku logs --tail`.
* Pour Voir la Base De Donnee __PRODUCTION__ et Voir Ce Que Vous Y mettez Dedans il Faut : <br>
-- Installer  [MongoDB Compass](https://www.mongodb.com/products/compass "Logiciel MongoDB Compass"), Copier : `mongodb+srv://<username>:<password>@cluster0-savma.mongodb.net/test`, puis le Logiciel Detectera la copie et Determinera seul les Options Pour se Connecter.

---
#### __POSTMAN__ : Au lieu de Refaire Les Requetes Reprenez Cette Collection apres Avoir installer  [Postman]( https://www.getpostman.com/ "Site Officiel") :
* https://www.getpostman.com/collections/78161031896675f095d7
Par Invitation :
* https://app.getpostman.com/join-team?invite_code=4a8cae8745c707e48132f6b6f3f05157
---
## N'hesitez Pas A Signaler N'importe Qu'elle Erreur meme Sur La Documentation.
---
## Adresses Heroku
#### [Git Heroku](https://git.heroku.com/greta-todoapp.git "adresse git heroku")
 * https://git.heroku.com/greta-todoapp.git

#### [URL Heroku](https://greta-todoapp.herokuapp.com "Publique")
 * https://greta-todoapp.herokuapp.com
---
## Ressources utilisees :
* [Documentation Mongodb](http://mongodb.github.io/node-mongodb-native/3.1/api/Collection.html "Site Officiel")
* [Cours complet Par Andrew Mead](https://www.udemy.com/the-complete-nodejs-developer-course-2/ "Lecon Udemy par Andrew Mead")
* [HTTP Code CRUD](https://httpstatuses.com/ "Liste complete des codes HTTP")
* [Documentation Express](http://expressjs.com/en/api.html#app.use "Site web Express")
* [Documentation librairie Mongoose, pour la __structure__ de notre db](https://mongoosejs.com/docs/connections.html "Site web Mongoose")
* [Promises](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise "Promise MDN")
* [Callbacks](https://developer.mozilla.org/en-US/docs/Glossary/Callback_function "Callbacks MDN")
* [Markdown Documentation](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Github Doc")
* [Liste Header HTTP](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields "Wikipedia")
---
## Autres Ressources Utilisees :
* [Differences SQL/noSQL]( https://docs.mongodb.com/manual/reference/sql-comparison/ "Par MongoDB")
---
## Liste Des NPM utilises :
* [Express](https://www.npmjs.com/package/express "Vers NPM")
* [Chalk](https://www.npmjs.com/package/chalk "Vers NPM") (desactive pour le moment)
* [Mongoose](https://www.npmjs.com/package/mongoose "Vers NPM")
* [Mongodb](https://www.npmjs.com/package/mongodb "Vers NPM")
* [Validator](https://www.npmjs.com/package/validator "Vers NPM")
* [Nodemon](https://www.npmjs.com/package/nodemon "Vers NPM")
* [Bcryptjs](https://www.npmjs.com/package/bcryptjs "Vers NPM")
* [Jsonwebtoken (__JWT__)](https://www.npmjs.com/package/jsonwebtoken "Vers NPM")
* [Multer](https://www.npmjs.com/package/multer "Vers NPM")
* [Sharp](https://www.npmjs.com/package/sharp "Vers NPM")
* [SendGrid](https://www.npmjs.com/package/@sendgrid/mail "Vers NPM")
* [Env-cmd](https://www.npmjs.com/package/env-cmd "Vers NPM")
* [Jest](https://www.npmjs.com/package/jest "Vers NPM")
* [SuperTest](https://www.npmjs.com/package/supertest "Vers NPM") (par Express)

## Liste Des Programmes Utilises :
* [MongoDB Compass](https://www.mongodb.com/products/compass "Logiciel MongoDB Compass") (__Recommande__)
* [Robot 3T]( https://robomongo.org/ "Site Officiel") (__Non Recommande__)
* [Postman]( https://www.getpostman.com/ "Site Officiel")
* [MongoDB Atlas]( https://www.mongodb.com/cloud/atlas "Site Officiel")
* [Heroku CLI]( https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up "Site Officiel")

---
## Commandes de Developpement  :
* __`npm run start`__ --> __node__.
* __`npm run dev`__ --> __nodemon__.
* __`npm run test`__ --> __Jest__.

---
##### Pour Utiliser Ce Repo Il Est Imperatif De Creer Un Fichier `.env` __Dans un Dossier `config`__ Car c'est lui qui sera Ignore Qui contiendra Les Variables D'environnement Suivante :
* `PORT` port 3000 pour dev
* ` SENDGRID_API_KEY ` API KEY Pour la Messagerie Send Grid
* `JSON_SECRET` Phrase Secrete Pour JSONToken
* `MONGODB_URL` Adresse Url De la BAse de donnees, par exemple `mongodb://127.0.0.1:27017/[nomDeLaDB]`
* Le script `npm run dev` Run ensuite la commande `env-cmd -f ./config/dev.env nodemon src/index.js` pour Lier le fichier avec notre Applcation<br>

##### Il est important A Noter Qu'il Y a un `dev.env` et un `test.env`. Cela Permet  d'Eviter A Jest, Lors Des Tests, De Remplir Notre Base De Donnee `dev`, on On Aura Donc Deux Base de Donnees

---
## Procedure de Debuggage ;

1. Pour __Chrome__ : [Procedure Debugging NodeJS Chrome]( https://medium.com/@paul_irish/debugging-node-js-nightlies-with-chrome-devtools-7c4a1b95ae27 "Procedure Pour debugg avec Chrome").
2. Pour __Firefox__ : [Procedure Debugging NodeJS Firefox]( https://nodejs.org/en/docs/guides/debugging-getting-started/ "Procedure Pour debugg avec Firefox").
---
### Procedure Pour __Mettre en Place__ et __Tester__ la Base de Donnee :
* Installer [Robot 3T]( https://robomongo.org/ "Site Officiel").
* Installer [Postman]( https://www.getpostman.com/ "Site Officiel").
* Creer un Dossier __Vide__ ou sera stocke la base de donner, nomme `task-manager-db`.
* Taper la commande `[cheminOuSeTrouveMongod.exe] --dbpath=[cheminDeLaBaseDeDonnees]` .
* Se Connecter a sa Base de Donnee via __Robot 3T__ (*test* puis *connect*).
* Lancer le script pour demarrer le serveur `npm run dev`.
---
### Couleur Message Log (Nodemon, Node) (Ceci a Ete enlever car causant des Erreur sur le Serveur de Deploiement, pour l'instant les Logs sont Non personnalise):
* __Chalk__ donne la possibilite de __personnaliser__ les `log` dans nodeJS, Voici leur liste:
* __Rouge__ : __Erreur__ Serveur.
* __Vert__ : Serveur __En Ecoute__.
* __Bleu__ : Requete __GET__ Recue.
* __Jaune__ : Requete __POST__ Recue.
* __Violet__ : Requete __UPDATE__ Recu.
* __Blanc__ : __Info__ Traitement Effectue.
---
### Variables D'environnement :
Le Dossier `config` contient Toutes les Variables D'environnement Necessaire Au __Dev__, pour __Production__ c'est Sur le site Heroku ou En ligne de Commande Qu'il faut Les attribuer.

#### Commande pour Set Une Variable D'Environnement sur __PRODUCTION__: `heroku config:set key:value`
Pour voir les variables Set : `heroku config`
#### Puis Faire : `git push heroku master`
---
#### Commandes Supplementaires __Heroku__:
* `heroku open` Ouvre Une page Web sur Le Site de Deploiement __PRODUCTION__
* `heroku logs --tail` Affihe les Logs Du Server
* `heroku ps:scale web=1` Verifier Qu'au Moins Une Instance Est en cours D'execution
---
### Deploiement :
#### Base De Donnee :
* Notre Base De Donnee Est Installe Sur un __Cluster__ (Multiple Serveurs, Faible Latence)  :
[MongoDB Atlas]( https://cloud.mongodb.com/v2/5cf133559ccf64b1fc97be9a#clusters?fastPoll=true "Notre Base De Donnees __MongoDB__").
* Pour Se connecter a MongoDB Atlas Le Mot De Passe Est `greta-2019!` et l'Username `axel`
* Pour Se connecter a La Base de Donnee en __PRODUCTION__ Il faut me Demander le Mot De Passe, le Repo Etant Publique Pour le Moment.

---
* __IMPORTANT__ Pour Une Utilisation en __DEVELOPPEMENT__ Il Faut au Prealable Faire Comme Deja Ecrit plus Haut Dans la Section `Procedure Pour Mettre en Place et Testerla Base de Donnee :`
* Creer un Dossier __Vide__ ou sera stocke la base de donner.
* Taper la commande `[cheminOuSeTrouveMongod.exe] --dbpath=[cheminDuDossierVideCreer]` .
---
#### Notre Application Possede Comme Dans Toutes Application Professionnelle Une Mise En Test, delivre par le Framework [JEST]( https://jestjs.io/ "Framework de Test Back-end")
* Le Dossier `test` comporte L'ensemble des Tests ecrit.
* [Documentation API Jest]( https://jestjs.io/docs/en/expect "Documentation de Jest")
*  Les __mocks__ sont des Librairies, ici Fourni Par [JEST]( https://jestjs.io/ "Site Offciel") Qui Permettent de Modifier Certains Modules Ou Pages De Notre Application Pour Notre Phase De Test (Par Exemple Comme Lors De L'envoi Des Emails Lorsqu'un Utilisateur Creer Un Compte Chez Nous). Le Dossier s'appelle `__mocks__` et C'est La bas Que [JEST]( https://jestjs.io/ "Site Offciel"). Donc Lorsqu'un Fichier Est 'Mocker' le Fichier Original Ne Sera Pas Pris En Compte Par [JEST]( https://jestjs.io/ "Site Offciel"), Seulement Le Fichier Dans Le Dossier `__mocks__`.
* [Documentation Mock JEST]( https://jestjs.io/docs/en/manual-mocks "Documentation de Jest")
* Toutes Idees Supplementaires Pour La Mise En Testest A Signaler Sur Notre Documentation Ou Sur le Slack


### Pourquoi Tester ?
* Tester Une Application Permet de Creer Une Application __Durable__ et __Strucure__ au fur Et A Mesure que notre Application S'aggrandit. Il Deviendra __Impossible de Devoir Tester une Application Seul__ Au fur Et A Mesure Que l'on creer Des Fonctionnalites. <br>
* Les Tests Sont La Pour Aider le Developpeur A Verifier Que Sont Travail N'a Pas Casser Certaines Parties Du Code. Par Exemple Lorsqu'il Refactorise Une Partie Du Code Il Doit Pouvoir Verifier que Rien N'a Ete casser Avant de Pouvoir Le Pousser En Production.<br>
*  De Plus c'est Tres Utile Lorsque L'on Travaille En Collaboration, Une personne Exterieure qui Vient Contribuer Au code, N'aura pas Toutes Les Connaissances Du Code que Celle Qui L'a Creer, Ainsi Meme S'il N'a Pas une connaissance Parfaite du Sujet, Il pourra y Contribuer Sans Risquer D'Implementer Des bugs.<br>
*  Les Tests Donnent Egalement La possibilite De Fournir Les Performances au Fur Et A mesure De L'ecriture De notre Code.
