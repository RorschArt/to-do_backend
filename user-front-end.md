# Liste des __Requetes__ et des __Headers__ pour Notre __API__ Pour la Collection  __USER__
---
## __User__
###  Creer Un  Utilisateur -> `url/users`
#### POST

* JSON a envoyer
```javascript
"name": "Lucifer",
"age": 3000, // > 0
"email": "hell@hotmail.com", //OBLIGATOIRE
"password": "hellboy" //(5 caracetere minimum) , OBLIGATOIRE
```

* En reponse __201__ vous obtiendrais :
```javascript
"user": {
        "age": 3000,
        "_id": "5cef736cb28e523064fcfd32",
        "createdAt": "2019-05-30T08:06:54.687Z",
        "updatedAt": "2019-05-30T08:06:55.186Z",
        "name": "Lucifer",
        "email": "hell@hotmail.com",
        "__v": 1
    },
    "token": "eyJhbGci.eyIiLCJjR9.8jps8vHanY" //de la session courante
}
```
> Durant la Creation Un Email De bienvenue Est Envoye
---
###  Login Un  Utilisateur -> `url/users/login`
#### POST

* JSON a envoyer
```javascript
{
	"email": "lit4p44sgsjjss44@gmail.com",
	"password": "killPHP"
}
```

* En reponse __200__ vous obtiendrais :
```javascript
{
    "user": {
        "age": 3000,
        "_id": "5cef7ec5de1ccf33b84f22d2",
        "name": "Lucifer",
        "email": "hell@hotmail.com",
        "createdAt": "2019-05-30T08:06:54.687Z",
        "updatedAt": "2019-05-30T08:06:55.186Z",
        "__v": 2
    },
    "token": "eyJhb9.eyJfaQzZ9.isuE"
}
```
---
###  Logout Tous les Utilisateurs (*d'un meme compte*)-> `url/users/logoutAll`
#### POST

* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

* En reponse __200__ vous obtiendrais :
```javascript
RIEN :)
```
---
###  Logout Un  Utilisateur -> `url/users/logout`

* JSON a envoyer
```javascript
RIEN :)
```

* En reponse __201__ vous obtiendrais :
```javascript
RIEN :)
```
---
###  Lire Un Profil  Utilisateur -> `url/users/me`
#### GET
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __201__ vous obtiendrais :
```javascript
{
    "age": 3000,
    "_id": "5cefbf543c7b922e50e9996d",
    "name": "Lucifer",
    "email": "hell@hotmail.com",
    "createdAt": "2019-05-30T11:32:36.309Z",
    "updatedAt": "2019-05-30T11:38:08.248Z",
    "__v": 4
}
```
---
###  Mettre a Jour Un Profil  Utilisateur -> `url/users/me`
#### PATCH
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
{
	"email":"hellforphp@gmail.com",
	"password": "phpisdead2019",
	"age": "22332"
}
```

* En reponse __201__ vous obtiendrais :
```javascript
{
    "age": 22332,
    "_id": "5cef7ec5de1ccf33b84f22d2",
    "name": "Lucifer",
    "createdAt": "2019-05-30T11:32:36.309Z",
    "updatedAt": "2019-05-30T11:38:08.248Z",
    "email": "hellforphp@gmail.com",
    "__v": 4
}
```
---
###  Supprimer Un Profil  Utilisateur -> `url/users/me`
#### DELETE
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __201__ vous obtiendrais :
```javascript
{
	"email":"hellforphp@gmail.com",
	"password": "phpisdead2019",
	"age": "22332"
}
```
> Durant la Suppression Un Email De bienvenue Est Envoye
---
