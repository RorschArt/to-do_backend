# Liste des __Requetes__ et des __Headers__ pour Notre __API__ Pour la Collection  __TASK__
---
## __Task__
###  Creer Une  Tache -> `url/tasks`
#### POST

* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
{
	"intitule": " KILL PHP ! by Lucifer" //minimum 3 OBLIGATOIRE
    "fait": "true" //NON-REQUIS, par defaut FALSE
}
```

En reponse __201__ vous obtiendrais :
```javascript
{
    "intitule": "KILL PHP ! by Lucifer",
    "fait": false,
    "createdAt": "2019-05-30T08:13:37.206Z",
    "updatedAt": "2019-05-30T08:13:37.206Z",
    "_id": "5cef87c0de1ccf33b84f22d6",
    "owner": "5cef7ec5de1ccf33b84f22d2",//Correspond a l'_id de l'utilisateur
    "__v": 0
}
}
```
---
###  Lire Une  Tache -> `url/tasks/:id`
#### GET
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __200__ vous obtiendrais :
```javascript
{
    "intitule": "KILL PHP ! by Lucifer",
    "fait": false,
    "_id": "5cef87c0de1ccf33b84f22d6",
    "createdAt": "2019-05-30T11:32:36.309Z",
    "updatedAt": "2019-05-30T11:38:08.248Z",
    "owner": "5cef7ec5de1ccf33b84f22d2",
    "__v": 0
}
```
---
###  Lire __Toutes__ les Taches d'un Utilisateur -> `url/tasks`
#### GET
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __200__ vous obtiendrais :
```javascript
[
    {
        "intitule": "KILL PHP ! by Lucifer",
        "fait": false,
        "_id": "5cef87c0de1ccf33b84f22d6",
        "createdAt": "2019-05-30T11:32:36.309Z",
        "updatedAt": "2019-05-30T11:38:08.248Z",
        "owner": "5cef7ec5de1ccf33b84f22d2",
        "__v": 0
    },
    {
        "intitule": "Send belzebut To Bring PHP Here",
        "fait": false,
        "_id": "5cef8999de1ccf33b84f22d7",
        "createdAt": "2019-05-30T11:32:36.309Z",
        "updatedAt": "2019-05-30T11:38:08.248Z",
        "owner": "5cef7ec5de1ccf33b84f22d2",
        "__v": 0
    }
]
```
---
###  Mettre A Jour Une Tache -> `url/tasks/:id`
#### PATCH
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
{
"intitule": "Nope Still Have to Kill him :)",
"fait": "true"
}
```

En reponse __200__ vous obtiendrais :
```javascript
{
    "intitule": "Nope Still Have to Kill him :)",
    "fait": true,
    "_id": "5cef8999de1ccf33b84f22d7",
    "createdAt": "2019-05-30T11:32:36.309Z",
    "updatedAt": "2019-05-30T11:38:08.248Z",
    "owner": "5cef7ec5de1ccf33b84f22d2",
    "__v": 0
}
```
---
###  Supprimer Une Tache -> `url/tasks/:id`
#### DELETE
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __200__ vous obtiendrais :
```javascript
{
    "intitule": "KILL PHP ! by Lucifer",
    "fait": false,
    "_id": "5cef87c0de1ccf33b84f22d6",
    "createdAt": "2019-05-30T11:32:36.309Z",
    "updatedAt": "2019-05-30T11:38:08.248Z",
    "owner": "5cef7ec5de1ccf33b84f22d2",
    "__v": 0
}
```
---
###  Lire les Taches d'un Utilisateur __Selon Des Criteres__-> `url/tasks`
#### GET
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* Dans l'__URL__ de la Requete On peut Ajouter des __Parametres__ (__Non Obligatoire__) de Cette Maniere :
`url/tasks` *`?key=value`*  :
* Liste des Parametres Possibles (__OPTIONNEL__) :
```javascript
//trie les taches par leur propriete task.fait (true ou false)
?fait=true/false
// limit et skip permette ensemble de pouvoir
//choisir quel tache on charge, ici cela voudrais dire que l'on
// va chercher seulement les taches de 10 a 20 (on skip les 10 premieres)
?limit=10&skip=10
//trier par ordre croissant ou decroissant depuis une propriete
//ici on trie de maniere croissante depuis le critere de date de creation (createdAt)
?sortBy=createdAt:asc
```

* JSON a envoyer
```javascript
RIEN :)
```

En reponse __200__ vous obtiendrais :
```javascript
[
    {
        "intitule": "KILL PHP ! by Lucifer",
        "fait": false,
        "_id": "5cef87c0de1ccf33b84f22d6",
        "createdAt": "2019-05-30T11:32:36.309Z",
        "updatedAt": "2019-05-30T11:38:08.248Z",
        "owner": "5cef7ec5de1ccf33b84f22d2",
        "__v": 0
    },
    {
        "intitule": "Send belzebut To Bring PHP Here",
        "fait": false,
        "_id": "5cef8999de1ccf33b84f22d7",
        "createdAt": "2019-05-30T11:32:36.309Z",
        "updatedAt": "2019-05-30T11:38:08.248Z",
        "owner": "5cef7ec5de1ccf33b84f22d2",
        "__v": 0
    }
]
```
---
