    //NPM
const request = require('supertest')
    //On recupere la configuration express SANS le 'listen'
const app = require('../src/app')
    //On recupere le modele de User
const User = require('../src/models/user')
const { setUpDb, userOne, userOneId } = require ('./fixtures/db')
    //Il est tres facile d'ecrrire des centaines de lignes de code pour nos test
    //on va donc eviter d'ecrire des tests inutiles, et se focaliser sur les tests
    //reellement obvious, apres cela reste un point de vue et doit etre discuter
    //au prealable

    // On utilise Jest pour creer notre test, Il est installer dans devDependecies
    // Il n'y a pas besoind e l'appeler JEST nous fourni Des methods et functions
    //en Globales.
    //@url:https://jestjs.io/docs/en/api

    //Avant Chaques test on defini un comportement(JEST) il existe egalement
    //'afterEach()' qui permet d'executer un code apres chaque tests
    //Comme toujours on utilise await/async(promise) car ce sont des operations
    //asynchrones
beforeEach(setUpDb)

test('Ce Test Devrait Inscrire Un Nouvel Utilisateur', async () => {
    // On utilise superTest (creer par express) pour pouvoir creer nos requetes
    // pour les test( sans Postman) SANS les Listen d'express
    //send() permet de specifier le body de la requetes
    //@url:https://www.npmjs.com/package/supertest
    const response = await request(app)
        .post('/users')
        .send({
            email: 'gretaDevAreBest@hotmail.fr',
            password: 'phpIDontLikeYouSorry',
            name: 'Linus'
        })
        .expect(201)
    // On veux s'assurer que l'utilisateur a bien ete enregistrer dans la DB
    const user = await User.findById(response.body.user._id)
    //On veut parametrer JEST pour qu'il s'attende a recevoir qqch dans la constante
    //c'est a dire que mongoose a trouve l'utilisateur
    //@url:https://jestjs.io/docs/en/expect#not
    //@url:https://jestjs.io/docs/en/expect#tobenull
    expect(user).not.toBeNull()
    //on s'assure qu'en reponse on a bien les bonne propriete de l'utilisateur
    //@url:https://jestjs.io/docs/en/expect#tomatchobjectobject
    expect(response.body).toMatchObject({
        user: {
            name: 'Linus',
            email: 'gretaDevAreBest@hotmail.fr',
        },
        token: response.body.token
    })
    //On s'assure que le mot de passe n'est pas Enregistrer en clair
    expect(user.password).not.toBe('phpIDontLikeYouSorry')
    //On s'assure que le Token enregistrer en Base est le meme que celui de la reponse
    expect(user.tokens[0].token).toBe(response.body.token)
})

test('Ce Test Devrait Connecter Un utilisateur', async () => {
    const response = await request(app)
        .post('/users/login')
        .send({
            email: userOne.email,
            password: userOne.password
        })
        .expect(200)

    //on Va chercher l'utilisateur qui vient de se connecter, sur la DB
    const user = await User.findById(userOne._id)

    // On verifie que le token creer lors de la connnexion, est bien enregistre dans la BD
    expect(response.body.token).toBe(user.tokens[1].token)
})

test('Ne devrait pas Connecter D\'utilisateur Non Inscrit', async () => {
    await request(app)
        .post('/users/login')
        .send({
            email: userOne.email,
            password: 'paslebon'
        })
        .expect(400)
})

test('Devrait Recuperer Les Infos D\'un Utilisateur inscrit', async () => {
    await request(app)
        .get('/users/me')
        //On set un header a notre requete avec 'set()''
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    })

test('Devrait Echouer Lorsqu\'on souhaite recuperer un profil d\'un user non-identifier', async () => {
    await request(app)
    .get('/users/me')
    .send()
    .expect(401)
})

test('Devrait Pouvoir Supprimer Un Utilisateur Authentifier', async () => {
    await request(app)
        .delete('/users/me')
        .set('Authorization',`${userOne.tokens[0].token}`)
        .send()
        .expect(200)

        //On va chercher l'utilisateur qui etait login
        const user = await User.findById(userOneId._id)

        //On verifie qu'il a ete supprime dans la BD
        expect(user).toBeNull()
})

test('Ne Devrait pas Passer Si un Utilisateur Non-identifier essaye de Supprimer un compte', async () => {
    await request(app)
        .delete('/users/me')
        .send()
        .expect(401)
})

test('Devrait Pouvoir Recevoir Une Image Uploade', async () => {
    await request(app)
        .post('/users/me/avatar')
        .set('Authorization', `${userOne.tokens[0].token}`)
        //on fake l'upload de l'image, le chemin commence depuis le root
        .attach('avatar', 'test/fixtures/profile-pic.jpg')
        .expect(201)

    const user = await User.findById(userOne._id)
    //! Pure Javascript ici tobe() utilise la triple egalite === par exemple {} === {}
    // retourne false. Il faut donc utiliser toEqual()
    //@url:https://jestjs.io/docs/en/expect#toequalvalue
    //Puis on verifie que dans notre base on a bien un buffer dans la prop 'avatar'
    //@URL:https://jestjs.io/docs/en/expect#tostrictequalvalue
    expect(user.avatar).toStrictEqual(expect.any(Buffer))

})

test('Devrait Pouvoir Update Seulement Des Proprietes Possible', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            name:'UpdatedName'
        })
        .expect(200)

        const user = await User.findById(userOne._id)
    //Utiliser toBe() irait tres bien aussi
        expect(user.name).toEqual('UpdatedName')
})

test('Ne Devrait pas Pouvoir Update des Proprietes Non Changeable', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            hobbies: 'tuPeuxPas'
        })
        .expect(404)
})
