    //On separe le fichier qui creer la base de donnee lors du test car on a besoin
    //des meme donnees durant nos test pour l'utilisateur et les taches puisque
    //qu'un utilisateur possede des taches. Ainsi on a juste a importer dans chaque
    //fichier la fonction qui creer la base de donnee dans le 'beforeEach()' et
    //on aura le meme modele de base de donnee, cela permet d'avoir un code mieux
    //structure, evolutif et plus lisible

    //models
const Task = require('../../src/models/task')
const User = require('../../src/models/user')
    //NPM
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
    //On utilise Mongoose pour creer un id que l'on stocke dans une variable car
    //on se servira de cet _id dans plusieurs parties du code.
const userOneId = new mongoose.Types.ObjectId()
    // On creer un utilisateur qui sera present lors de notre dans lequel il est
    //appliquer, comme pour login un utilisateur par exemple

const userOne = {
    _id: userOneId,
    name: "php",
    email: "phpisnothereanymore@hotmail.fr",
    password: 'iKnowImDeadYeah',
    tokens:[{
        token:
            jwt.sign({ _id: userOneId }, process.env.JSON_SECRET)
    }]
}

const userTwoId = new mongoose.Types.ObjectId()
    // On creer un utilisateur qui sera present lors de notre dans lequel il est
    //appliquer, comme pour login un utilisateur par exemple

const userTwo = {
    _id: userTwoId,
    name: "javascript",
    email: "javascriptishere@hotmail.fr",
    password: "sorryPhp",
    tokens:[{
        token:
            jwt.sign({ _id: userTwoId }, process.env.JSON_SECRET)
    }]
}

const taskOne = {
    _id: new mongoose.Types.ObjectId(),
    intitule: "Keep Evolving",
    completed: false,
    owner: userTwoId
}

const taskTwo = {
    _id: new mongoose.Types.ObjectId(),
    intitule: "Trying to Not Be Too Hard On The Old PHP",
    completed: true,
    owner: userTwoId
}

const taskThree = {
    _id: new mongoose.Types.ObjectId(),
    intitule: "Fight Until Im Dead, even If Im Dead Already",
    completed: true,
    owner: userOneId
}

const setUpDb = async () => {
    await Task.deleteMany()
    await User.deleteMany()

    await new User(userOne).save()
    await new User(userTwo).save()

    await new Task(taskOne).save()
    await new Task(taskTwo).save()
    await new Task(taskThree).save()
}

module.exports = {
    setUpDb,
    userOne,
    userOneId,
    userTwo,
    userTwoId,
    taskOne
}
