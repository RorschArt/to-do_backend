//Model
const Task = require('../src/models/task')
//NPM
const request  = require('supertest')
const app = require('../src/app')
//Db
const { setUpDb,
        userOne,
        userOneId,
        userTwo,
        userTwoId,
        taskOne
     } = require('./fixtures/db')

beforeEach(setUpDb)
    console.log(taskOne)

test('Un Utilisateur Devrait Pouvoir Creer Une Tache',  async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            intitule: "Essayer d'eviter d'apprendre PHP"
        })
        .expect(201)

    const task = await Task.findById(response.body._id)

    expect(task).not.toBeNull()
    //On verifie que la tache creer a bien eut l'intitule donne dans la requete
    expect(task.intitule).not.toBe('Nouvelle Tache')
    expect(task.fait).toBe(false)
})

test('Devrait pouvoir Afficher Toutes les Taches D\'un Utilisateur', async () => {
    const response = await request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(200)
        //On verifie que l'on a bien assigne 2 Taches a L'utilisateur

        // expect(tasks).toStrictEqual(expect.any(Array))
        expect(response.body.length).toEqual(2)
        //On verifie que la tache a ete supprimee
})

test('Ne Devrait Pas Pouvoir Supprimer La Tache D\'un Autre Utilisateur', async () => {
    const response = await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        expect(401)

    //On verifie que la tache n'a pas ete supprimee
    const task = await Task.findById(taskOne._id)

    expect(task).not.toBeNull()
})
