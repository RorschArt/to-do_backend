# Liste des Requetes pour l'Upload des fichiers

### Lors du Deploiement d'une Base de Donnees sur Un Server les fichiers sont automatiquement Effacer, donc on ne les Stocke Jamais en 'Dur' Dans la Base. Ainsi le Modele User Possede une Propriete `Avatar` qui Est En Realite un Buffer(Binaire)
---
## Les Images Avatar Sont Prealablement Traiter Dans un Format Standart : `.png`
## Upload D'un Avatar pour un User -> `url/users/me/avatar`
## __POST__
* ## Requete :
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`
* Dans le __`body`__ de la requete Vous Devrez Envoyer :
`form-data`avec `avatar`: `le fichier a uploader`

* ## Specificites

Taille limite : __10 MB__ <br>
Types de Fichier Possible : `.png .jpg .svg .jpeg`<br>
En reponse __201__ vous obtiendrais :
```javascript
AUCUN :)
```
---
## Suppression D'un Avatar pour un User -> `url/users/me/avatar`
## __DELETE__
* ## Requete :
* Dans la requete vous devrez donner en `header` :
`"Authorization": "Bearer eyJhbGci.eyIiLCJjR9.8jps"`

* JSON a envoyer
```javascript
AUCUN :)
```
---
## Afficher l'Avatar d'un Utilisateur Depuis (Sans Identification) -> `url/users/:id/avatar`
__GET__
* JSON a envoyer
```javascript
AUCUN :)
```
---
